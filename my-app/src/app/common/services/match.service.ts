import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators'
import { Match } from '../interfaces/match';
@Injectable({
  providedIn: 'root'
})
export class MatchService {

  constructor(private http : HttpClient) { }

  list(): Observable<Match[]>{
    return this.http.get(environment.apiBaseUrl + 'matches').pipe(
      map((item: any)=>{
        return item
      })
    )
  }

  show(id: number): Observable<Match>{
    return this.http.get(environment.apiBaseUrl + 'matches/' + id).pipe(
      map((item:any)=>{
        return item;
      })
    )

  }
}
